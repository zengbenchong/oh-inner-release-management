

# Openharmony_1.0.1.009转测试信息

版本号： Openharmony_1.0.1.009 
Release版本：
LTS_L1_Aries_hi3518-copy-release-3518
http://download.ci.openharmony.cn/version/Release_Version/Openharmony_1.0.1.009/20220714_010116/version-Release_Version-Openharmony_1.0.1.009-20220714_010116-LTS_L1_Aries_hi3518-copy-release-3518.tar.gz

LTS_L1_Taurus_hi3516-copy-release-3516
http://download.ci.openharmony.cn/version/Release_Version/Openharmony_1.0.1.009/20220714_010110/version-Release_Version-Openharmony_1.0.1.009-20220714_010110-LTS_L1_Taurus_hi3516-copy-release-3516.tar.gz

LTS_L0_Pegasus_wifiiot-copy-release-wifi
http://download.ci.openharmony.cn/version/Release_Version/Openharmony_1.0.1.009/20220714_010120/version-Release_Version-Openharmony_1.0.1.009-20220714_010120-LTS_L0_Pegasus_wifiiot-copy-release-wifi.tar.gz

Debug版本：
LTS_L1_Aries_hi3518
http://download.ci.openharmony.cn/version/Release_Version/Openharmony_1.0.1.009/20220714_010117/version-Release_Version-Openharmony_1.0.1.009-20220714_010117-LTS_L1_Aries_hi3518.tar.gz

LTS_L1_Taurus_hi3516
http://download.ci.openharmony.cn/version/Release_Version/Openharmony_1.0.1.009/20220714_010121/version-Release_Version-Openharmony_1.0.1.009-20220714_010121-LTS_L1_Taurus_hi3516.tar.gz

LTS_L0_Pegasus_wifiiot
http://download.ci.openharmony.cn/version/Release_Version/Openharmony_1.0.1.009/20220714_010123/version-Release_Version-Openharmony_1.0.1.009-20220714_010123-LTS_L0_Pegasus_wifiiot.tar.gz



# Openharmony_1.0.1.007转测试信息 #

转测试版本号：OpenHarmony_1.0.1.007
版本用途：tag版本,v1.1.4
转测试时间：2022/1/24
版本获取路径：
Release版本：
LTS_L1_Aries_hi3518-copy-release-3518
http://download.ci.openharmony.cn/version/Release_Version/Openharmony_1.0.1.007/20220124_131057/version-Release_Version-Openharmony_1.0.1.007-20220124_131057-LTS_L1_Aries_hi3518-copy-release-3518.tar.gz

LTS_L1_Taurus_hi3516-copy-release-3516
http://download.ci.openharmony.cn/version/Release_Version/Openharmony_1.0.1.007/20220124_130458/version-Release_Version-Openharmony_1.0.1.007-20220124_130458-LTS_L1_Taurus_hi3516-copy-release-3516.tar.gz


LTS_L0_Pegasus_wifiiot-copy-release-wifi
http://download.ci.openharmony.cn/version/Release_Version/Openharmony_1.0.1.007/20220124_130058/version-Release_Version-Openharmony_1.0.1.007-20220124_130058-LTS_L0_Pegasus_wifiiot-copy-release-wifi.tar.gz

Debug版本：
LTS_L1_Aries_hi3518
http://download.ci.openharmony.cn/version/Release_Version/Openharmony_1.0.1.007/20220124_133058/version-Release_Version-Openharmony_1.0.1.007-20220124_133058-LTS_L1_Aries_hi3518.tar.gz


LTS_L1_Taurus_hi3516
http://download.ci.openharmony.cn/version/Release_Version/Openharmony_1.0.1.007/20220124_132459/version-Release_Version-Openharmony_1.0.1.007-20220124_132459-LTS_L1_Taurus_hi3516.tar.gz

LTS_L0_Pegasus_wifiiot
http://download.ci.openharmony.cn/version/Release_Version/Openharmony_1.0.1.007/20220124_132103/version-Release_Version-Openharmony_1.0.1.007-20220124_132103-LTS_L0_Pegasus_wifiiot.tar.gz





# Openharmony_1.0.1.006转测试信息 #

转测试版本号：OpenHarmony_1.0.1.006
版本用途：内测版本
转测试时间：2022/1/12
版本获取路径：
release：
hi3516 L1 Taurus：
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_1.0.1.006/20220112_120100/version-Release_Version-OpenHarmony_1.0.1.006-20220112_120100-LTS_L1_Taurus_hi3516-copy-release-3516.tar.gz

hi3518 L1 Aries: 
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_1.0.1.006/20220112_141206/version-Release_Version-OpenHarmony_1.0.1.006-20220112_141206-LTS_L1_Aries_hi3518-copy-release-3518.tar.gz

hi3861 L0 Pegasus:
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_1.0.1.006/20220112_141151/version-Release_Version-OpenHarmony_1.0.1.006-20220112_141151-LTS_L0_Pegasus_wifiiot-copy-release-wifi.tar.gz

debug:
hi3518 L1 Aries:
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_1.0.1.006/20220112_125104/version-Release_Version-OpenHarmony_1.0.1.006-20220112_125104-LTS_L1_Aries_hi3518.tar.gz

hi3861 L0 Pegasus wifiiot
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_1.0.1.006/20220112_124606/version-Release_Version-OpenHarmony_1.0.1.006-20220112_124606-LTS_L0_Pegasus_wifiiot.tar.gz

hi3516 L1 Taurus: 
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_1.0.1.006/20220112_115559/version-Release_Version-OpenHarmony_1.0.1.006-20220112_115559-LTS_L1_Taurus_hi3516.tar.gz

# OpenHarmony_1.0.1.005转测试信息 #
转测试版本号：OpenHarmony_1.0.1.005
版本用途：标签tag版本
转测试时间：2021/9/15
版本获取路径：
debug:
LTS_L0_Pegasus_wifiiot:
version url: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_release_1.0.1.005/20210915_010723/version-Release_Version-OpenHarmony_release_1.0.1.005-20210915_010723-hispark_pegasus.tar.gz
LTS_L1_Taurus_hi3516:
version url: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_release_1.0.1.005/20210915_010131/version-Release_Version-OpenHarmony_release_1.0.1.005-20210915_010131-hispark_taurus.tar.gz
LTS_L1_Aries_hi3518:
version url: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_release_1.0.1.005/20210915_005246/version-Release_Version-OpenHarmony_release_1.0.1.005-20210915_005246-hispark_aries.tar.gz
release:
LTS_L1_Aries_hi3518-copy-release-3518:
version url: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_release_1.0.1.005/20210915_001709/version-Release_Version-OpenHarmony_release_1.0.1.005-20210915_001709-hispark_aries.tar.gz
LTS_L1_Taurus_hi3516-copy-release-3516:
version url: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_release_1.0.1.005/20210915_001114/version-Release_Version-OpenHarmony_release_1.0.1.005-20210915_001114-hispark_taurus.tar.gz
LTS_L0_Pegasus_wifiiot-copy-release-wifi:
version url: http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_release_1.0.1.005/20210915_000432/version-Release_Version-OpenHarmony_release_1.0.1.005-20210915_000432-hispark_pegasus.tar.gz

# OpenHarmony_1.0.1.004转测试信息 #
转测试版本号：OpenHarmony_1.0.1.004
版本用途：标签tag版本
转测试时间：2021/7/16
版本获取路径：
Release版本
3518
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_release_1.0.1.004/20210716_102326/version-Release_Version-OpenHarmony_release_1.0.1.004-20210716_102326-hispark_aries.tar.gz
3516
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_release_1.0.1.004/20210716_103019/version-Release_Version-OpenHarmony_release_1.0.1.004-20210716_103019-hispark_taurus.tar.gz
wifi
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_release_1.0.1.004/20210716_103455/version-Release_Version-OpenHarmony_release_1.0.1.004-20210716_103455-hispark_pegasus.tar.gz

debug版本
3518
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_release_1.0.1.004/20210716_125612/version-Release_Version-OpenHarmony_release_1.0.1.004-20210716_125612-hispark_aries.tar.gz
3516
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_release_1.0.1.004/20210716_120141/version-Release_Version-OpenHarmony_release_1.0.1.004-20210716_120141-hispark_taurus.tar.gz
wifi
http://download.ci.openharmony.cn/version/Release_Version/OpenHarmony_release_1.0.1.004/20210716_124941/version-Release_Version-OpenHarmony_release_1.0.1.004-20210716_124941-hispark_pegasus.tar.gz

# OpenHarmony_1.0.1.003转测试信息 #
转测试版本号：OpenHarmony_1.0.1.003
版本用途：标签tag版本
转测试时间：2021/6/4
版本获取路径：
3518
https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Release_Version/OpenHarmony_1.0.1.003/20210604_143816/version-Release_Version-OpenHarmony_1.0.1.003-20210604_143816-hispark_aries.tar.gz

3516
https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Release_Version/OpenHarmony_1.0.1.003/20210604_143844/version-Release_Version-OpenHarmony_1.0.1.003-20210604_143844-hispark_taurus.tar.gz

wifiiot
https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Release_Version/OpenHarmony_1.0.1.003/20210604_143559/version-Release_Version-OpenHarmony_1.0.1.003-20210604_143559-hispark_pegasus.tar.gz

# OpenHarmony_release_1.0.1_sp1转测试信息 #
转测试版本号：OpenHarmony_release_1.0.1_sp1
版本用途：内测
转测试时间：2021/4/26
版本获取路径：
3516：
https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Release_Version/OpenHarmony_release_1.0.1_sp2/2021-04-20_09-35-33/hispark_taurus.tar.gz
3518：
https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Release_Version/OpenHarmony_release_1.0.1_sp2/2021-04-19_16-57-40/hispark_aries.tar.gz
wifiiot：
https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Release_Version/OpenHarmony_release_1.0.1_sp2/2021-04-19_14-44-30/hispark_pegasus.tar.gz
